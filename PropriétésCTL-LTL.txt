**Model Checking**
not deadlock

**CTL Formulas**
"if a floor is requested, it will be reached, the door will open" (not functional)
AG ( buttons_in(i) or buttons_out(i) => EF current_floor = i & motor_state = stopped & door_state = open & lift_state = stop & AX (not buttons_in(i) & not buttons_out(i)) )
AG ( {buttons_in(0) = TRUE or buttons_out(0) = TRUE } => EF {current_floor = 0 & motor_state = stopped & door_state = open & lift_state = stop} )


"the lift does not infinitely move in one direction (up or down)"
AG ( {lift_state = up or lift_state = down} => AF ( { lift_state = stop} ) )


**LTL Formulas**
"the lift cannot go higher than floor N or lower than floor 0"
{current_floor = N} => not X {lift_state = up}
{current_floor = 0} => not X {lift_state = down}


